package jdbc;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;


public class JdbcRoleDao extends AbstractjdbcDao implements RoleDao {
    Properties prop;

    public JdbcRoleDao() {
        prop = new Properties();
        try {
            prop.load(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    Connection createConnection() {
        try {
            Class.forName(prop.getProperty("db.driver"));
            return DriverManager.getConnection(prop.getProperty("db.url"), prop.getProperty("db.username"), prop.getProperty("db.password"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public void create(Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("INSERT INTO ROLE VALUES(" + role.getId() + " , '"
                    + role.getName() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("UPDATE ROLE SET NAME='" + role.getName()
                    + "' WHERE ID=" + role.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(Role role) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM ROLE" + " WHERE ID=" + role.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Role findByName(String name) {
        Role role = new Role();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM ROLE WHERE NAME='"
                     + name + "';")) {
            resultSet.next();
            role.setId(resultSet.getLong("ID"));
            role.setName(resultSet.getString("NAME"));
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return role;
    }
}
