package jdbc;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class JdbcUserDao extends AbstractjdbcDao implements UserDao {
    Properties prop;

    public JdbcUserDao() {
        prop = new Properties();
        try {
            prop.load(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Connection createConnection() {
        try {
            Class.forName(prop.getProperty("db.driver"));
            return DriverManager.getConnection(prop.getProperty("db.url"), prop.getProperty("db.username"), prop.getProperty("db.password"));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public void create(User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("INSERT INTO USER VALUES(" + user.getId() + " , '"
                    + user.getLogin() + "', '"
                    + user.getPassword() + "', '" + user.getEmail() + "', '" + user.getFirstName()
                    + "', '" + user.getLastName() + "', '" + user.getBirthDay() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("UPDATE USER SET LOGIN='" + user.getLogin() + "', PASSWORD='"
                    + user.getPassword() + "', EMAIL='" + user.getEmail() + "', FIRSTNAME='" + user.getFirstName()
                    + "', LASTNAME='" + user.getLastName() + "', BIRTHDAY='" + user.getBirthDay()
                    + "' WHERE ID=" + user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remove(User user) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM USER" + " WHERE ID=" + user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> findAll() {
        List<User> allUsers = new ArrayList<>();
        User user = null;
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM USER;")) {
            while (resultSet.next()) {
                user = new User();
                initItems(user, resultSet);
                allUsers.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return allUsers;
    }

    @Override
    public User findByLogin(String login) {
        User user = new User();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM USER WHERE LOGIN='" + login + "';")) {
            resultSet.next();
            initItems(user, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return user;
    }

    @Override
    public User findByEmail(String email) {
        User user = new User();
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM USER WHERE EMAIL='" + email + "';")) {
            resultSet.next();
            initItems(user, resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        return user;
    }

    void initItems(User user, ResultSet resultSet) throws SQLException {
        user.setId(resultSet.getLong("ID"));
        user.setLogin(resultSet.getString("LOGIN"));
        user.setPassword(resultSet.getString("PASSWORD"));
        user.setEmail(resultSet.getString("EMAIL"));
        user.setFirstName(resultSet.getString("FIRSTNAME"));
        user.setLastName(resultSet.getString("LASTNAME"));
        user.setBirthDay(resultSet.getString("BIRTHDAY"));
    }

//    public void createTable() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("CREATE TABLE USER\n" +
//                    "(\n" +
//                    "    ID BIGINT PRIMARY KEY,\n" +
//                    "    LOGIN VARCHAR(255),\n" +
//                    "    PASSWORD VARCHAR(255),\n" +
//                    "    EMAIL VARCHAR(255),\n" +
//                    "    FIRSTNAME VARCHAR(255),\n" +
//                    "    LASTNAME VARCHAR(255),\n" +
//                    "    BIRTHDAY VARCHAR(255)\n" +
//                    ");");
//            statement.execute("    CREATE TABLE ROLE\n" +
//                    "            (\n" +
//                    "                    ID BIGINT PRIMARY KEY,\n" +
//                    "                    NAME VARCHAR(255),\n" +
//                    ");");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

}
